package ru.citeck.ecos.context.lib.auth

object AuthConstants {

    const val APP_PREFIX = "APP_"
    const val GROUP_PREFIX = "GROUP_"

    const val SYSTEM_USER = "system"
}
